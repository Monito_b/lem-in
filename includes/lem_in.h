/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 17:18:54 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:07:48 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <libft.h>

# define NUM  "0123456789"

typedef struct		s_tube
{
	int				id_1;
	int				id_2;
	struct s_tube	*next;
}					t_tube;

typedef struct		s_lemin
{
	int				nbr_ants;
	char			*name;
	int				is_start;
	int				is_end;
	int				is_saloon;
	int				id_s;
	struct s_lemin	*next;
}					t_lemin;

typedef struct		s_anthill
{
	t_lemin			*start;
	t_lemin			*end;
	t_lemin			*list;
	t_tube			*list_tube;
	int				**matrix;
	int				total_ants;
	int				id;
}					t_anthill;

void			get_saloon(char *line, t_anthill *home);
void			get_end(char *line, t_anthill *home);
void			get_start(char *line, t_anthill *home);
void			get_links(char *line, t_anthill *home);
void			get_comments(char *line, t_anthill *home);
int				ft_is_comment(char *s, int c);
t_lemin			*creat_element(char *saloon);
void			ft_push_back_list(t_lemin *new_el, t_lemin **saloon);
int				lent_list(t_lemin **list);
int				ft_is_link(char *s);
int				get_key(char *line);
int				**ft_init_matrix(t_anthill *home);
void			ft_zero_matrix(t_anthill *home, int **matrix);
int				get_id(char *saloon, t_anthill *home);
void			ft_fill_matrix(int **matrix, int id_1, int id_2);
void			filling_links_tab(t_anthill *home);
void			ft_print_matrix(int **marix, int len_matrix);

#endif