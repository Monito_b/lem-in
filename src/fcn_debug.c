/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fcn_debug.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 23:31:43 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:32:45 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void ft_print_matrix(int **matrix, int len_matrix)
{
	int	i;
	int	j;

	i = 0;
	while (i < len_matrix)
	{
		j = 0;
		while (matrix[i][j] != -1)
		{
			ft_putnbr(matrix[i][j]);
			write(1, " ", 1);
			j++;
		}
		write(1, "\n", 1);
		i++;
	}
}
