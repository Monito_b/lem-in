/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   type_key.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/22 16:47:14 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 22:09:36 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static int	ft_count_double(char *str, char c)
{
	int		i;
	int		is_double;

	i = 0;
	is_double = 0;
	while (str[i] != '\0')
	{
		if (str[i] == c)
			is_double++;
		i++;
	}
	return (is_double);
}

static int	ft_is_tube(char *line)
{
	int		i;

	i = 0;
	if (ft_count_double(line, '-') > 2)
		return (0);
	while (line[i] != '\0')
	{
		if (line[i] == '-' || line[i] == '\t')
			return (1);
		i++;
	}
	return (0);
}

static int	get_key2(char *line)
{
	if (line[0] != '#')
	{
		if (ft_is_tube(line) == 1)
			return (4);
		else
			return (3);
	}
	return (5);
}

int			get_key(char *line)
{
	if (line[0] == '#' && line[1] == '#')
	{
		if ((ft_strncmp("##start", line, 7) == 0)
				&& ft_is_comment(line, '#') == 0)
			return (1);
		else if ((ft_strncmp("##end", line, 5) == 0)
				&& (ft_is_comment(line, '#') == 0))
			return (2);
	}
	return (get_key2(line));
}