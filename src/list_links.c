/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_links.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 18:42:52 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 20:28:45 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

static t_tube		*creat_element_tube(int  link1, int link2)
{
	t_tube	*new_el;

		if (!(new_el = (t_tube *)malloc(sizeof(t_tube))))
			return (NULL);
		new_el->id_1 = link1;
		new_el->id_2 = link2;
		new_el->next = NULL;
		return (new_el);
}

static void			ft_push_back_tube(t_tube *new_el, t_tube **links)
{
	t_tube *temp;

	temp = *links;
	if (temp == NULL)
	{
		*links = temp;
		return ;
	}
	while (temp->next != NULL)
	{
		temp = temp->next;
	}
	temp->next = new_el;
}

void		get_links(char *line, t_anthill *home)
{
	t_tube	*link_l;
	char	**links;
	int		id_1;
	int		id_2;

	links = NULL;
	links = ft_strsplit(line, '-');
	link_l = NULL;
	ft_putstr(links[0]);
	write(1, " ", 1);
	ft_putendl(links[1]);
	id_1 = get_id(links[0], home);
	id_2 = get_id(links[1], home);
	if (home->list_tube == NULL)
	{
		home->list_tube = creat_element_tube(id_1, id_2);
	}
	else
	{
		link_l = creat_element_tube(id_1, id_2);
		ft_push_back_tube(link_l, &(home->list_tube));
	}
}
