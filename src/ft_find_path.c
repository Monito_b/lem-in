/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_path.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 20:17:25 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:34:52 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

#include <stdio.h>

static int	*ft_find_path(t_anthill *home, int x, int *path, int deap)
{
	int		counter;
	int		*temp_path;

	counter = 0;
	while (home->matrix[x][counter] != -1)
	{
		if (home->matrix[x][counter] == 1 && path[counter] == 0)
		{
			path[counter] = deap;
			temp_path = ft_find_path(home, counter, path, deap + 1);
			if (temp_path != NULL)
				return (temp_path);
			path[counter] = 0;
		}
		counter++;
	}
	if (x == home->end->id_s)
	{
		ft_putstr("WIN!\n");
			return (path);
	}
	int		i;
	i = 0;
	while (i <= lent_list(&(home->list)))
	{
		write(1, "path-> ", 6);
		ft_putnbr(path[i]);
		i++;
	}
	write(1, "\n", 1);
	return (NULL);
}

void		filling_links_tab(t_anthill *home)
{
	t_tube	*temp;
	int		*path;

	temp = home->list_tube;
	if (!(path = (int *)malloc(sizeof(int) * lent_list(&(home->list) + 1))))
		return ;
	while (temp != NULL)
	{
		ft_fill_matrix(home->matrix, temp->id_1, temp->id_2);
		temp = temp->next;
	}
	path[home->start->id_s] = 1;
	ft_putnbr(home->start->id_s);
	write(1, " ", 1);
	ft_putnbr(home->end->id_s);
	write(1, "\n", 1);
	path = ft_find_path(home, home->start->id_s, path, 2);
	ft_print_matrix(home->matrix, lent_list(&(home->list)));
}
