/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_start.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 05:22:54 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 22:00:25 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			get_start(char *line, t_anthill *home)
{
	char		**start;
	char		*str;

	start = NULL;
	if (get_next_line(0, &str) < 1)
		return ;
	start = ft_strsplit(str, ' ');
	ft_putendl(str);
	if (home->start == NULL)
	{
		home->start = creat_element(start[0]);
		ft_push_back_list(home->start, &home->list);
		home->start->nbr_ants = home->total_ants;
	}
	else if (home->start != NULL)
	{
		ft_putstr_fd("only one start please", 1);
		exit(0);
	}
	free(str);
	(void)line;
}