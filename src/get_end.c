/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_end.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/18 17:12:45 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:10:31 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void		get_comments(char *line, t_anthill *home)
{
	(void)line;
	(void)home;
}

int			get_id(char *saloon, t_anthill *home)
{
	t_lemin	*temp;

	temp = home->list;
	while (temp != NULL)
	{
		if (ft_strcmp(saloon, temp->name) == 0)
			return (temp->id_s);
		temp = temp->next;
	}
	return (0);
}

void		get_end(char *line, t_anthill *home)
{
	char	**end;
	char	*str;

	end = NULL;
	if (get_next_line(0, &str) < 1)
		return ;
	end = ft_strsplit(str, ' ');
	ft_putendl(str);
	if (home->end == NULL)
	{
		home->end = creat_element(end[0]);
		ft_push_back_list(home->end, &home->list);
	}
	else if (home->end != NULL)
	{
		ft_putstr_fd("only one start please", 1);
		exit(0);
	}
	free(str);
	(void)line;
}
