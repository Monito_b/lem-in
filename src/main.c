/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 17:18:27 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:08:25 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_anthill	*ft_init(t_anthill *ants)
{
	if ((ants = (t_anthill *)malloc(sizeof(t_anthill)))  == NULL
			|| (ants->end = (t_lemin *)malloc(sizeof(t_lemin))) == NULL
			|| (ants->list_tube = (t_tube *)malloc(sizeof(t_tube))) == NULL)
		exit (0);
	ants->start = NULL;
	ants->end = NULL;
	ants->list = NULL;
	ants->list_tube = NULL;
	ants->id = 0;
	return (ants);
}

void		get_information(t_anthill *ants)
{
	char	*start;
	int		ret;
	void	(*fontions_list[6])(char *, t_anthill *) =
	{get_start, get_end, get_saloon, get_links, get_comments};

	ret = 0;
	while ((ret = get_next_line(0, &start)))
	{
		(*fontions_list[(get_key(start)) - 1])(start, ants);
		free(start);
		if (ret == 0)
			break ;
	}
	ants->matrix = ft_init_matrix(ants);
	filling_links_tab(ants);
	free(start);
}

int				main(void)
{
	char		*line;
	t_anthill	*ants;

	ants = NULL;
	ants = ft_init(ants);
	if (get_next_line(0, &line) < 1)
		return (1);
	if ((ft_strcmp(line, "\0")) || (ft_strcmp(line, "0")
		&& !ft_strcmp(line, "\n")))
	{
		ants->total_ants = ft_atoi(line);
		if (ants->total_ants == 0)
			return (0);
		ft_putendl(line);
	}
	else
	{
		ft_putstr_fd("Not ants found", 2);
		write(1, "\n", 1);
		exit (0);
	}
	free(line);
	get_information(ants);
	return (0);
}
