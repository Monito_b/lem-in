/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fcn_1.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 17:48:32 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 20:35:12 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void			get_saloon(char *line, t_anthill *home)
{
	if (line[0] != '#' && line[1] != '#')
	{
		char		**saloon;
		t_lemin		*s_saloon;

		saloon = NULL;
		s_saloon = NULL;
		saloon = ft_strsplit(line, ' ');
		if (line[0] == '\0')
			return ;
		ft_putendl(line);
		if (home->list == NULL)
		{
			home->list = creat_element(saloon[0]);
		}
		else
		{
			s_saloon = creat_element(saloon[0]);
			ft_push_back_list(s_saloon, &((home)->list));
		}
		free(saloon);
	}
}

void ft_fill_matrix(int **matrix, int id_1, int id_2)
{
	matrix[id_1][id_2] = 1;
	matrix[id_2][id_1] = 1;
}

