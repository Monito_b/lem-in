/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   creat_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/23 18:38:44 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:51:36 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int			lent_list(t_lemin **list)
{
	t_lemin *temp;
	int		i;

	i = 0;
	temp = *list;
		if (temp == NULL)
		{
			ft_putstr("error");
			exit(0);
		}
	while (temp != NULL)
	{
		temp = temp->next;
		i++;
	}
	return (i);
}

t_lemin		*creat_element(char *saloon)
{
	t_lemin *new_el;

	if (!(new_el = (t_lemin *)malloc(sizeof(t_lemin))))
		return (NULL);
	new_el->name = saloon;
	new_el->next = NULL;
	new_el->is_start = 0;
	new_el->is_end = 0;
	new_el->is_saloon = 1;
	new_el->id_s = 0;
	return (new_el);
}

void		ft_push_back_list(t_lemin *new_el, t_lemin **saloon)
{
	t_lemin *temp;
	int		counter;

	temp = *saloon;
	counter = 1;
	if (saloon)
	{
		if (temp == NULL)
		{
			*saloon = new_el;
			return ;
		}
		while (temp->next != NULL)
		{
			temp = temp->next;
			counter++;
		}
		temp->next = new_el;
		new_el->is_start = 0;
		new_el->is_saloon = 0;
		new_el->is_end = 0;
		new_el->id_s = counter;
	}
}
