/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   creat_matrix.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbernabe <jbernabe@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/03/23 17:55:41 by jbernabe          #+#    #+#             */
/*   Updated: 2014/03/23 23:18:53 by jbernabe         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int				**ft_init_matrix(t_anthill *home)
{
	int		len_matrix;
	int		**matrix;
	int		i;
	int		j;

	i = 0;
	len_matrix = lent_list(&(home->list)) + 1;
	if (!(matrix = (int **)malloc(sizeof(int *) * len_matrix + 2)))
		return (0);
	while (i < len_matrix + 1)
	{
		if (!(matrix[i] = (int *)malloc(sizeof(int) * len_matrix + 2)))
			return (0);
		j = 0;
		while (j < len_matrix)
		{
			matrix[i][j] = 0;
			j++;
		}
		matrix[i][j] = -1;
		i++;
	}
	matrix[len_matrix] = NULL;
	ft_print_matrix(matrix, len_matrix);
	return (matrix);
}
